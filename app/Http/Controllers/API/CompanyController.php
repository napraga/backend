<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Company;
use Validator;
use App\Http\Resources\Company as CompanyResource;
use App\User;
use App\Token;
use Hash;
use Auth;
use DateTime;
   
class CompanyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::guard('api')->user()->id;
        $company = Company::all()->where('user_id', $user_id);
        return $this->sendResponse(CompanyResource::collection($company), 'Company retrieved successfully.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $company = Company::all();
        return $this->sendResponse(CompanyResource::collection($company), 'Company retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'nit' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input["user_id"] = Auth::guard('api')->user()->id;
        $company = Company::create($input);
   
        return $this->sendResponse(new CompanyResource($company), 'Company created successfully. ');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
  
        if (is_null($company)) {
            return $this->sendError('Company not found.');
        }
   
        return $this->sendResponse(new CompanyResource($company), 'Company retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'nit' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $company->nit = $input['nit'];
        $company->name = $input['name'];
        $company->address = $input['address'];
        $company->phone = $input['phone'];
        $company->save();
   
        return $this->sendResponse(new CompanyResource($company), 'Company updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
   
        return $this->sendResponse([], 'Company deleted successfully.');
    }
}