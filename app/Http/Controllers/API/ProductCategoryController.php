<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\ProductCategory;
use Validator;
use App\Http\Resources\ProductCategory as ProductCategoryResource;
   
class ProductCategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ProductCategory::all();
        return $this->sendResponse(ProductCategoryResource::collection($categories), 'Categories retrieved successfully.');
    }
   
}