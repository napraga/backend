<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
   
class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'address' => 'required',
            'country' => 'required',
            'city' => 'required',
            // 'user_type_id' => 'required|integer',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['user_type_id'] = (isset($input['user_type_id']))?$input['user_type_id']:1;
        $user = User::create($input);
        $success['token'] =  $user->createToken('Shop')->accessToken;
        $success['first_name'] =  $user->first_name;
        $success['last_name'] =  $user->last_name;
        $success['address'] =  $user->address;
        $success['country'] =  $user->country;
        $success['city'] =  $user->city;
        $success['user_type_id'] =  $user->user_type_id;
        return $this->sendResponse($success, 'User register successfully.');
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('Shop')->accessToken; 
            $success['first_name'] =  $user->first_name;
            $success['last_name'] =  $user->last_name;
            $success['address'] =  $user->address;
            $success['country'] =  $user->country;
            $success['city'] =  $user->city;
            $success['user_type_id'] =  $user->user_type_id;
            return $this->sendResponse($success, 'User login successfully.');
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        } 
    }
}