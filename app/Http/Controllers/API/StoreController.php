<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Store;
use Validator;
use App\Http\Resources\Store as StoreResource;
use App\User;
use App\Token;
use Hash;
use Auth;
use DateTime;
   
class StoreController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::guard('api')->user()->id;
        
        $store = Store::all();//->where('user_id', $user_id);

        return $this->sendResponse(StoreResource::collection($store), 'store retrieved successfully.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $store = Store::all();
        return $this->sendResponse(StoreResource::collection($store), 'store retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'nit' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'company_id' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        // $input["user_id"] = Auth::guard('api')->user()->id;
        $store = Store::create($input);
        return $this->sendResponse(new StoreResource($store), 'store created successfully. ');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = Auth::guard('api')->user()->id;
        $store = Store::all()->where('company_id', $id);
        return $this->sendResponse(StoreResource::collection($store), 'store retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'nit' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'company_id' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $store->nit = $input['nit'];
        $store->name = $input['name'];
        $store->address = $input['address'];
        $store->phone = $input['phone'];
        $store->company_id = $input['company_id'];
        $store->save();
   
        return $this->sendResponse(new StoreResource($store), 'store updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        $store->delete();
        return $this->sendResponse([], 'store deleted successfully.');
    }
}