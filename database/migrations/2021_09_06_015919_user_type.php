<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        DB::table('user_type')->insert(
            array(
                'name' => 'User'
            )
        );

        DB::table('user_type')->insert(
            array(
                'name' => 'Customer'
            )
        );

        DB::table('user_type')->insert(
            array(
                'name' => 'Admin'
            )
        );

        DB::table('user_type')->insert(
            array(
                'name' => 'Super-Admin'
            )
        );
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_type');
    }
}
