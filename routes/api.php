<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\ProductCategoryController;
use App\Http\Controllers\API\MyProductController;
use App\Http\Controllers\API\MyAccountController;
use App\Http\Controllers\API\CompanyController;
use App\Http\Controllers\API\StoreController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);
Route::get('products', [ProductController::class, 'all']);
Route::get('products_category/{category}', [ProductController::class, 'filterCategory']);
Route::get('product_id/{id}', [ProductController::class, 'filterId']);
Route::get('products_search/{criterio}', [ProductController::class, 'filterSearch']);
Route::get('categories', [ProductCategoryController::class, 'index']);
Route::get('stores', [StoreController::class, 'all']);

Route::middleware('auth:api')->group( function () {
    Route::resource('product', ProductController::class);
    Route::resource('myaccount', MyAccountController::class);
    Route::resource('mycompanies', CompanyController::class);
    Route::resource('mystores', StoreController::class);
    Route::resource('myproduct', MyProductController::class);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
